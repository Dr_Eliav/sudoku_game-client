
package sudoku_game.client;

import java.io.*;
import java.net.*;
import ObjectsSerialization.Information;
import Enums.*;

/**
 * This class creates a new request to server
 * the class receives a request and information about the same request
 * will create a socket connection to the server 
 * and returns the new information from the server to the client 
 * and returns if success or failed
 * 
 * @author EliavBuskila
 */
public class ServerConnection {
    
    private static final int PORT = 5056;
    private static final String IP = "localhost";
    
    private Socket socket;
    private ObjectOutputStream oos;
    private ObjectInputStream ois;
    
    private Information data;

    // contractor - create new request
//    public ServerConnection(Object obj, Request request) {
//        data = new Information(request, null, obj);
//    }
//    public ServerConnection(Object obj1, Object obj2, Request request) {
//        data = new Information(request, null, obj1, obj2);
//    }
    public  ServerConnection(Request request, Object ... objs) {
        data = new Information(request, null, objs);
    }
    
    // send data to server
    private void output() throws IOException{
        oos = new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject(this.data);
        oos.flush();
    }
    // get data from server
    private void input() throws IOException, ClassNotFoundException{
        ois = new ObjectInputStream(socket.getInputStream());
        this.data = (Information) ois.readObject();
    }
    // close connection with server
    private void close() throws IOException{
        oos.close();
        ois.close();
    }
    
    public Information newServerRequest(){
        try {
            // start connection wuth server
            socket = new Socket(IP, PORT);
            output();
            input();
            close();
            return data;
            
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println("newServerRequest - error");
        }
        data.setResult(Result.ERROR_CLIENT);
        return null;
    }   
    public Result getResult(){
        return this.data.getResult();
    }
    
    public String toString(){
        return "info: " + this.data.toString();
    }
}
