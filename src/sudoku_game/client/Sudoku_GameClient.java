
package sudoku_game.client;
import screens.LoginScreen;

/**
 *
 * @author EliavBuskila
 */
public class Sudoku_GameClient {

    public static void main(String[] args) {
        
        LoginScreen ls = new LoginScreen();
        ls.setLocationRelativeTo(null);
        ls.setVisible(true);
    }
}
