
package screens;

import Enums.Request;
import Enums.Result;
import ObjectsSerialization.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.Stack;
import javax.swing.JButton;
import sudoku_game.client.ServerConnection;

/**
 *
 * @author EliavBuskila
 */
public class GameScreen extends javax.swing.JFrame {

    private ServerConnection sc; // for requests
    private Game game; // board + playr
    private int digit;
    private JButton[] digits; // visual digits choice
    private JButton[][] board; // visual board
    private Stack<Integer> undo; // undo  (i,j,digit)  
    private Runnable sound; // play sound
    private Thread printTime;
    private boolean printTimeFlag;
    private long startGameTime;

    public GameScreen() {
        initComponents();
    }
    // for playing
    public GameScreen(Player player, Board board) {
        initComponents();
        initVariables();
        this.game.setPlayer(player);
        this.game.setBoard(board);
        this.btnSolve.setVisible(false);
        
        initSound();
        createGame(player, board);
        startTime();
        createVisualBoard();
        createVisualDigits();
        printBoard(game.getSaveBoard());
    }
    // for solver
    public GameScreen(Player player) {
        initComponents();
        initVariables();
        this.game.setPlayer(player);
        this.btnSave.setVisible(false);
        this.btnCheck.setVisible(false);
        
        initSound();
        createVisualBoard();
        createVisualDigits();

    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblBoard = new javax.swing.JLabel();
        btnSolve = new javax.swing.JButton();
        btnEraser = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        pnlDigits = new javax.swing.JPanel();
        btnSave = new javax.swing.JButton();
        btnUndo = new javax.swing.JButton();
        btnCheck = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        msgBox = new javax.swing.JTextField();
        txtTime = new javax.swing.JTextArea();
        lblBackground = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        lblBoard.setIcon(new javax.swing.ImageIcon("C:\\Users\\EliavBuskila\\Desktop\\Study\\Sudoku_Game\\JAVA\\image\\board.png")); // NOI18N
        lblBoard.setOpaque(true);

        btnSolve.setBackground(new java.awt.Color(204, 204, 204));
        btnSolve.setFont(new java.awt.Font("Algerian", 0, 24)); // NOI18N
        btnSolve.setText("Solve");
        btnSolve.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnSolve.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSolve.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSolveMouseClicked(evt);
            }
        });

        btnEraser.setBackground(new java.awt.Color(255, 255, 255));
        btnEraser.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnEraser.setText("Eraser");
        btnEraser.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnEraser.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEraser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEraserMouseClicked(evt);
            }
        });

        btnBack.setBackground(new java.awt.Color(204, 204, 204));
        btnBack.setFont(new java.awt.Font("Algerian", 0, 24)); // NOI18N
        btnBack.setText("Back");
        btnBack.setToolTipText("the game will not be saved");
        btnBack.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnBack.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBackMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout pnlDigitsLayout = new javax.swing.GroupLayout(pnlDigits);
        pnlDigits.setLayout(pnlDigitsLayout);
        pnlDigitsLayout.setHorizontalGroup(
            pnlDigitsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 499, Short.MAX_VALUE)
        );
        pnlDigitsLayout.setVerticalGroup(
            pnlDigitsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 58, Short.MAX_VALUE)
        );

        btnSave.setBackground(new java.awt.Color(204, 204, 204));
        btnSave.setFont(new java.awt.Font("Algerian", 0, 24)); // NOI18N
        btnSave.setText("Save");
        btnSave.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnSave.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSaveMouseClicked(evt);
            }
        });

        btnUndo.setBackground(new java.awt.Color(204, 204, 204));
        btnUndo.setFont(new java.awt.Font("Algerian", 1, 18)); // NOI18N
        btnUndo.setText("undo");
        btnUndo.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnUndo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnUndo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnUndoMouseClicked(evt);
            }
        });

        btnCheck.setBackground(new java.awt.Color(204, 204, 204));
        btnCheck.setFont(new java.awt.Font("Algerian", 0, 24)); // NOI18N
        btnCheck.setText("Check");
        btnCheck.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnCheck.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCheck.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCheckMouseClicked(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(204, 204, 204));
        btnClear.setFont(new java.awt.Font("Algerian", 1, 18)); // NOI18N
        btnClear.setText("clear");
        btnClear.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnClear.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnClear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnClearMouseClicked(evt);
            }
        });

        msgBox.setEditable(false);
        msgBox.setBackground(new java.awt.Color(204, 255, 255));
        msgBox.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        msgBox.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        msgBox.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 204, 204)));

        txtTime.setEditable(false);
        txtTime.setBackground(new java.awt.Color(204, 255, 255));
        txtTime.setColumns(20);
        txtTime.setFont(new java.awt.Font("Courier New", 1, 36)); // NOI18N
        txtTime.setRows(5);
        txtTime.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 204, 204)));

        lblBackground.setIcon(new javax.swing.ImageIcon("C:\\Users\\EliavBuskila\\Desktop\\Study\\Sudoku_Game\\JAVA\\image\\background.jpg")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnSolve, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtTime, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnUndo, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 6, Short.MAX_VALUE))
                                    .addComponent(msgBox, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(72, 72, 72)))
                        .addComponent(lblBoard)
                        .addGap(145, 145, 145))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(pnlDigits, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnEraser, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(51, 51, 51))))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(lblBackground)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(txtTime, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblBoard)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pnlDigits, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnEraser, javax.swing.GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE))
                        .addGap(101, 101, 101))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(msgBox, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnBack, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                            .addComponent(btnUndo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnClear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(12, 12, 12)
                        .addComponent(btnCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSolve, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(lblBackground)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void initVariables() {
        this.game = new Game();
        this.digits = new JButton[Board.SIZE];
        this.board = new JButton[Board.SIZE][Board.SIZE];
        this.digit = 0;
        this.undo = new Stack<>();
    }
    
    private void btnDigitMouseClicked(java.awt.event.MouseEvent evt) {
        digit = Integer.valueOf(((JButton)evt.getSource()).getText());
        btnEraser.setBackground(Color.WHITE);
        for (int i = 0; i < Board.SIZE; i++) {
            digits[i].setBackground(Color.WHITE);
        }
        digits[digit-1].setBackground(new Color(0, 204, 204));
    }
    private void btnDigitBoardMouseClicked(java.awt.event.MouseEvent evt) {
        for (int i = 0; i < Board.SIZE; i++) {
            for (int j = 0; j < Board.SIZE; j++) {
                if (board[i][j] == evt.getSource()) {
                    undo.push(i);
                    undo.push(j);
                    undo.push(boardGetInt(board[i][j]));
                    boardSetText(board[i][j], digit);
                }
            }
        }
    }
    private void btnBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBackMouseClicked
        printTimeFlag = false;
        HomeScrenn hs = new HomeScrenn(game.getPlayer());
        hs.setLocation(this.getLocation());
        hs.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnBackMouseClicked
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        printTimeFlag = false;
        sc = new ServerConnection(Request.LOGOUT, game.getPlayer());
        sc.newServerRequest();
    }//GEN-LAST:event_formWindowClosing
    private void btnEraserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEraserMouseClicked
        digit = 0;
        btnEraser.setBackground(new Color(0, 204, 204));
        for (int i = 0; i < Board.SIZE; i++) {
            digits[i].setBackground(Color.WHITE);
        }
    }//GEN-LAST:event_btnEraserMouseClicked
    private void btnSolveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSolveMouseClicked
        Board solveBoard = new Board();
        scanBoard(solveBoard.getStrart());
        sc = new ServerConnection(Request.SOLVE_BOARD, solveBoard);
        solveBoard = (Board)sc.newServerRequest().getInfo();
        if (sc.getResult() == Result.SUCCESS) {
            printBoard(solveBoard.getSolution());
        } else {
            pringMsg(sc.getResult().toString());
        }   
    }//GEN-LAST:event_btnSolveMouseClicked
    private void btnSaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseClicked
        stopTime();
        scanBoard(game.getSaveBoard());
        sc = new ServerConnection(Request.UPDATE_GAME, game);
        sc.newServerRequest();
        btnBackMouseClicked(evt);
    }//GEN-LAST:event_btnSaveMouseClicked
    private void btnUndoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUndoMouseClicked
        if (!undo.isEmpty()) {
            int oldDigit = undo.pop();
            int j = undo.pop();
            int i = undo.pop();
            System.out.println(i +","+j+"="+oldDigit);
            boardSetText(board[i][j], oldDigit);
        } else {
            pringMsg("no more undo!");
        }
    }//GEN-LAST:event_btnUndoMouseClicked
    private void btnCheckMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCheckMouseClicked
        if(scanBoard(game.getSaveBoard())) {
            sc = new ServerConnection(Request.CHECK_SOLUTION, game);
            game = (Game)sc.newServerRequest().getInfo();
            if (game.getIsSolved()) {
                stopTime();
                pringMsg("correct solution!");
                
                // save game
                scanBoard(game.getSaveBoard());                
                sc = new ServerConnection(Request.UPDATE_GAME, game);
                sc.newServerRequest();
                
                // go back
                HomeScrenn hs = new HomeScrenn(game.getPlayer());
                hs.setLocation(this.getLocation());
                hs.setVisible(true);
                
                // print msg
                GameOverScreen gos = new GameOverScreen(game);
                gos.setLocation(this.getLocation());
                gos.setLocationRelativeTo(null);
                gos.setVisible(true);
                this.dispose();
            } else {
                pringMsg("incorrect solution!");
                showMistakes();
            }
        } else {
            pringMsg("the board\nis not full!");
        }
    }//GEN-LAST:event_btnCheckMouseClicked
    private void btnClearMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnClearMouseClicked
        for (int i = 0; i < Board.SIZE; i++) {
            for (int j = 0; j < Board.SIZE; j++) {
                if (game.getBoard().getStrart(i, j) == 0) {
                    board[i][j].setText("");
                    board[i][j].setForeground(new Color(0, 204, 204));
                }
            }
        }
    }//GEN-LAST:event_btnClearMouseClicked

    private void boardSetText(JButton btn, int d) {
        if (d == 0) btn.setText("");
        else btn.setText(d+"");
    }
    private int boardGetInt(JButton btn){
        return (btn.getText().equalsIgnoreCase("")) ? (0) : (Integer.valueOf(btn.getText()));
    }
    private void initSound(){
        sound = (Runnable) Toolkit.getDefaultToolkit().getDesktopProperty("win.sound.exclamation");
    }
    private void pringMsg(String msg) {
        new Thread() {
            @Override
            public void run() {
                try {
                    sound.run();
                    msgBox.setText(msg);
                    Thread.sleep(1000*3);
                    msgBox.setText("");
                } catch (InterruptedException ex) {
                        System.err.println(ex);
                }
            }
        }.start();
    }
    private void createGame(Player player, Board board) {
        if (player.getIsUser()) {
            // check is exist save game for user
            sc = new ServerConnection(Request.GET_GAME, player, board, game);
            game = (Game)sc.newServerRequest().getInfo(2);
            
            if (game.getBoard().getStatus().equals("solved")) {
                msgBox.setText("Game Over!");
                //txtTime.setText(game.getStringGameTime());
                btnClear.setVisible(false);
                btnUndo.setVisible(false);
                btnCheck.setVisible(false);
                btnSave.setVisible(false);
            }
        } else {
            // guest never get save game
            this.game.setSaveBoard(board.getStrart());
            btnSave.setVisible(false);
        }
    }
    private void startTime() {       
        txtTime.setText(game.getStringGameTime());
        startGameTime = System.currentTimeMillis() - game.getGameTime();
        game.setGameTime(startGameTime);
        
        printTime = new Thread() {
            @Override
            public void run() {
                printTimeFlag = true;
                long time;
                while (!game.getIsSolved() && printTimeFlag) {
                    time = System.currentTimeMillis() - startGameTime;
                    txtTime.setText(
                            String.format("%02d:%02d:%02d.%03d",
                                    (time/(1000*60*60)%24),
                                    (time/(1000*60)%60),
                                    (time/1000%60),
                                    (time%1000)));
                }
                System.out.println("END");
            }
        };
        printTime.start();
    }
    private void stopTime() {
        printTimeFlag = false;
        game.setGameTime(System.currentTimeMillis()-startGameTime);
        System.out.println(game.getStringGameTime());
    }
    private void createVisualBoard(){
        lblBoard.setLayout(new GridLayout(Board.SIZE, Board.SIZE));
        for (int i = 0; i < Board.SIZE; i++) {
            for (int j = 0; j < Board.SIZE; j++) {
                board[i][j] = new JButton();
                board[i][j].setBackground(new java.awt.Color(255, 255, 255));
                board[i][j].setFont(new java.awt.Font("Tahoma", 1, 24));
                board[i][j].setBorder(null);
                board[i][j].setOpaque(false);
                if (game.getBoard().getStrart(i, j) == 0) {
                    board[i][j].setForeground(new Color(0, 204, 204));
                    board[i][j].setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                    board[i][j].addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                            btnDigitBoardMouseClicked(evt);
                        }
                    });
                }
                lblBoard.add(board[i][j]);
            }
        }
    }
    private void createVisualDigits(){
        pnlDigits.setLayout(new GridLayout(1, Board.SIZE));
        for (int i = 0; i < Board.SIZE; i++) {
            digits[i] = new JButton((i+1)+"");
            digits[i].setPreferredSize(new Dimension(50, 50));
            digits[i].setBackground(Color.WHITE);
            digits[i].setFont(new java.awt.Font("Tahoma", 1, 24));
            digits[i].setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
            digits[i].setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
            digits[i].addMouseListener(new java.awt.event.MouseAdapter() {
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        btnDigitMouseClicked(evt);
                    }
                });
            pnlDigits.add(digits[i]);
        }
    }
    private void printBoard(int[][] board) {
        for (int i = 0; i < Board.SIZE; i++) {
            for (int j = 0; j < Board.SIZE; j++) {
                boardSetText(this.board[i][j], board[i][j]);
            }
        }
    }
    private void showMistakes() {
        new Thread() {
            @Override
            public void run() {
                try {
                    sound.run();
                    for (int i = 0; i < Board.SIZE; i++) 
                        for (int j = 0; j < Board.SIZE; j++) 
                            if (game.getBoard().getStrart(i, j) == 0) 
                                board[i][j].setForeground(Color.RED);
                    Thread.sleep(1000*3);
                    for (int i = 0; i < Board.SIZE; i++) 
                        for (int j = 0; j < Board.SIZE; j++) 
                            if (game.getBoard().getStrart(i, j) == 0) 
                                board[i][j].setForeground(new Color(0, 204, 204));        
                } catch (InterruptedException ex) {
                    System.err.println(ex);
                }
            }
        }.start();
    }
    private boolean scanBoard(int[][] board) {
        boolean flag = true;
        for (int i = 0; i < Board.SIZE; i++) {
            for (int j = 0; j < Board.SIZE; j++) {
                board[i][j] = boardGetInt(this.board[i][j]);
                if(boardGetInt(this.board[i][j]) == 0) flag = false;
            }
        }
        return flag;
    }
    
    public static void main(String args[]) {    
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GameScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCheck;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnEraser;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSolve;
    private javax.swing.JButton btnUndo;
    private javax.swing.JLabel lblBackground;
    private javax.swing.JLabel lblBoard;
    private javax.swing.JTextField msgBox;
    private javax.swing.JPanel pnlDigits;
    private javax.swing.JTextArea txtTime;
    // End of variables declaration//GEN-END:variables
}
