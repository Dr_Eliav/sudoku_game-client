
package screens;

import Enums.*;
import ObjectsSerialization.User;
import java.awt.Cursor;
import sudoku_game.client.ServerConnection;

/**
 *
 * @author EliavBuskila
 */
public class RegisterScreen extends javax.swing.JFrame {

    private static ServerConnection sc;
    private User register;
    private boolean isVerified;
    
    public RegisterScreen() {
        initComponents(); 
        setVerifiedVisible(false);
        register = new User();
        isVerified = false;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        background = new javax.swing.JLabel();
        txtUsername = new javax.swing.JTextField();
        txtPass1 = new javax.swing.JPasswordField();
        txtPass2 = new javax.swing.JPasswordField();
        txtEmail = new javax.swing.JTextField();
        txtCode = new javax.swing.JTextField();
        btnOK = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        btnDone = new javax.swing.JButton();
        msgBox = new javax.swing.JTextArea();
        lblCode = new javax.swing.JLabel();
        lblEmail = new javax.swing.JLabel();
        lblPass2 = new javax.swing.JLabel();
        lblPass1 = new javax.swing.JLabel();
        lblUsername = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sudoku Gmae - Register");
        setIconImages(null);
        setName("LoginScreen"); // NOI18N
        setResizable(false);

        background.setIcon(new javax.swing.ImageIcon("C:\\Users\\EliavBuskila\\Desktop\\Study\\Sudoku_Game\\JAVA\\image\\background.jpg")); // NOI18N

        txtUsername.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtUsername.setForeground(new java.awt.Color(0, 204, 204));
        txtUsername.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtUsername.setToolTipText("must contain 5 to 25 characters");
        txtUsername.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtUsername.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtUsername.setNextFocusableComponent(txtPass1);

        txtPass1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtPass1.setForeground(new java.awt.Color(0, 204, 204));
        txtPass1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPass1.setToolTipText("must contain 8 to 25 characters");
        txtPass1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtPass1.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtPass1.setNextFocusableComponent(txtPass2);

        txtPass2.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtPass2.setForeground(new java.awt.Color(0, 204, 204));
        txtPass2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPass2.setToolTipText("must contain 8 to 25 characters");
        txtPass2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtPass2.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtPass2.setNextFocusableComponent(txtEmail);

        txtEmail.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtEmail.setForeground(new java.awt.Color(0, 204, 204));
        txtEmail.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtEmail.setToolTipText("Example@mail.com");
        txtEmail.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtEmail.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtEmail.setNextFocusableComponent(btnDone);

        txtCode.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtCode.setForeground(new java.awt.Color(0, 204, 204));
        txtCode.setNextFocusableComponent(btnOK);

        btnOK.setBackground(new java.awt.Color(204, 204, 204));
        btnOK.setFont(new java.awt.Font("Algerian", 0, 24)); // NOI18N
        btnOK.setText("OK");
        btnOK.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnOK.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnOK.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnOKMouseClicked(evt);
            }
        });

        btnBack.setBackground(new java.awt.Color(204, 204, 204));
        btnBack.setFont(new java.awt.Font("Algerian", 0, 24)); // NOI18N
        btnBack.setText("Back");
        btnBack.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnBack.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBackMouseClicked(evt);
            }
        });

        btnDone.setBackground(new java.awt.Color(204, 204, 204));
        btnDone.setFont(new java.awt.Font("Algerian", 0, 24)); // NOI18N
        btnDone.setText("Send me code");
        btnDone.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnDone.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDone.setNextFocusableComponent(txtCode);
        btnDone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDoneMouseClicked(evt);
            }
        });

        msgBox.setEditable(false);
        msgBox.setColumns(20);
        msgBox.setFont(new java.awt.Font("Courier New", 0, 20)); // NOI18N
        msgBox.setRows(5);
        msgBox.setText("Fill in the fields with an authentic email address \nand verify using the code that you got from \nyour email addresand press 'Done' below.");
        msgBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        lblCode.setFont(new java.awt.Font("Algerian", 1, 36)); // NOI18N
        lblCode.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCode.setText("insert code:");
        lblCode.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 204, 204)));

        lblEmail.setFont(new java.awt.Font("Algerian", 1, 36)); // NOI18N
        lblEmail.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblEmail.setText("E-mail:");
        lblEmail.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 204, 204)));

        lblPass2.setFont(new java.awt.Font("Algerian", 1, 36)); // NOI18N
        lblPass2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPass2.setText("password");
        lblPass2.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 204, 204)));

        lblPass1.setFont(new java.awt.Font("Algerian", 1, 36)); // NOI18N
        lblPass1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPass1.setText("password");
        lblPass1.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 204, 204)));

        lblUsername.setFont(new java.awt.Font("Algerian", 1, 36)); // NOI18N
        lblUsername.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUsername.setText("username:");
        lblUsername.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 204, 204)));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(140, 140, 140)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(lblEmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(lblPass2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(lblPass1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(lblUsername, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(txtPass2, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                    .addComponent(txtUsername)
                                                    .addComponent(txtPass1, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                    .addGap(146, 146, 146))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(lblCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(msgBox, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                                    .addGap(141, 141, 141)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(540, 540, 540)
                                .addComponent(btnDone, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(37, 37, 37))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(background)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(67, 67, 67)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblUsername, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPass1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPass1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPass2, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPass2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblEmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtEmail))
                .addGap(19, 19, 19)
                .addComponent(msgBox, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lblCode))
                    .addComponent(btnOK, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtCode, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(111, 111, 111)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDone, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(background)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void btnOKMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnOKMouseClicked
        if (register.isTrueCode(Double.parseDouble(txtCode.getText()))) {
            isVerified = true;
            msgBox.setText("Your email is verified, Press Done! to login.");
            setVerifiedVisible(false);
            btnDone.setText("Done!");
        } else {
            msgBox.setText("The code is incorrect.");
        }
    }//GEN-LAST:event_btnOKMouseClicked
    private void btnDoneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDoneMouseClicked
        if (!isVerified) { 
            if (txtPass1.getText().equalsIgnoreCase(txtPass2.getText())) {
                register.setUsername(txtUsername.getText());
                register.setPassword(txtPass1.getText());
                register.setEmail(txtEmail.getText());
                sendCode();
            } else {
                msgBox.setText("Passwords don't match");
            }
        } else {
            addUser(evt);
        }
    }//GEN-LAST:event_btnDoneMouseClicked
    private void btnBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBackMouseClicked
        LoginScreen ls = new LoginScreen();
        ls.setLocation(this.getLocation());
        ls.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnBackMouseClicked
    
    private void sendCode() {
        sc = new ServerConnection(Request.SEND_CODE, register);
        register = ((User)sc.newServerRequest().getInfo());
            
        if (sc.getResult() == Result.ERROR_SEND_EMAIL) {
            msgBox.setText("Please enter a valid Details");
        } else {
            setVerifiedVisible(true);
        }    
    }
    private void addUser(java.awt.event.MouseEvent evt) {
        sc = new ServerConnection(Request.ADD_USER, register);
        register = ((User)sc.newServerRequest().getInfo());
        
        if (sc.getResult() == Result.SUCCESS) {
            btnBackMouseClicked(evt);
        } else {
            msgBox.setText(sc.getResult().toString());
        }
    }
    private void setVerifiedVisible(boolean flag) {
        lblCode.setVisible(flag);
        txtCode.setVisible(flag);
        btnOK.setVisible(flag);
    }
    
    public static void main(String args[]) {
       java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegisterScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel background;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDone;
    private javax.swing.JButton btnOK;
    private javax.swing.JLabel lblCode;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblPass1;
    private javax.swing.JLabel lblPass2;
    private javax.swing.JLabel lblUsername;
    private javax.swing.JTextArea msgBox;
    private javax.swing.JTextField txtCode;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JPasswordField txtPass1;
    private javax.swing.JPasswordField txtPass2;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
}
