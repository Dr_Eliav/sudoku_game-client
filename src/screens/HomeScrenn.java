
package screens;

import Components.pnlChooseBoard;
import Enums.*;
import ObjectsSerialization.Board;
import ObjectsSerialization.Player;
import ObjectsSerialization.Statistic;
import java.awt.GridLayout;
import java.util.LinkedList;
import sudoku_game.client.ServerConnection;

/**
 *
 * @author EliavBuskila
 */
public class HomeScrenn extends javax.swing.JFrame {
    
    private ServerConnection sc;
    private Player player;
    private Statistic statistic;
    private LinkedList<Board> boards;
    
    public HomeScrenn() {
        initComponents();
    }
    public HomeScrenn(Player player) {
        initComponents();
        this.player = new Player(player);
        this.boards = new LinkedList<>();

        if (this.player.getIsUser()) {
            // get statistic
            this.statistic = new Statistic();
            sc = new ServerConnection(Request.GET_STATISTIC, this.player, this.statistic);
            this.statistic = (Statistic)sc.newServerRequest().getInfo(1);
            
            // set statistic
            lblUsername.setText(statistic.getUsername());
            lblTotalPlayingTime.setText(statistic.getTotalPlayingTime());
            lblSolvedGames.setText(statistic.getTotalSolvedGames());
            lblCompletion.setText(statistic.getCompletion());
        } else {
            lblUsername.setText(this.player.getUsername());
            btnOrder.setVisible(false);
            chooseOrder.setVisible(false);
            btnRandomGame.setVisible(false);
            chooseLevel.setVisible(false);
        }
        
        // get all boards
        sc = new ServerConnection(Request.GET_ALL_BOARDS, this.player, this.boards);
        this.boards = (LinkedList<Board>)sc.newServerRequest().getInfo(1);
        
        // create a visual list of all boards
        //this.sclpnlBoards.setPreferredSize(new Dimension(500, 580));
        this.pnlBoards.setLayout(new GridLayout((boards.size()<3)?(3):boards.size(),1));
        orderBoards("all boards");
    }
    public Player getPlayer() {
        return this.player;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Background = new javax.swing.JLabel();
        btnExit = new javax.swing.JButton();
        lblTitle = new java.awt.Label();
        sclpnlBoards = new javax.swing.JScrollPane();
        pnlBoards = new javax.swing.JPanel();
        lblUsername = new javax.swing.JTextField();
        lblTotalPlayingTime = new javax.swing.JTextField();
        lblSolvedGames = new javax.swing.JTextField();
        lblCompletion = new javax.swing.JTextField();
        btnRandomGame = new javax.swing.JButton();
        btnSolveMe = new javax.swing.JButton();
        chooseLevel = new javax.swing.JComboBox<>();
        btnOrder = new javax.swing.JButton();
        chooseOrder = new javax.swing.JComboBox<>();
        btnInstuctions = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sudoku Game  - Home");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        Background.setIcon(new javax.swing.ImageIcon("C:\\Users\\EliavBuskila\\Desktop\\Study\\Sudoku_Game\\JAVA\\image\\background.jpg")); // NOI18N

        btnExit.setBackground(new java.awt.Color(204, 204, 204));
        btnExit.setFont(new java.awt.Font("Algerian", 0, 24)); // NOI18N
        btnExit.setText("Exit");
        btnExit.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnExit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnExitMouseClicked(evt);
            }
        });

        lblTitle.setAlignment(java.awt.Label.CENTER);
        lblTitle.setBackground(new java.awt.Color(204, 255, 255));
        lblTitle.setFont(new java.awt.Font("Algerian", 1, 36)); // NOI18N
        lblTitle.setText("all boards");

        sclpnlBoards.setBackground(new java.awt.Color(204, 255, 255));
        sclpnlBoards.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sclpnlBoards.setOpaque(false);

        pnlBoards.setBackground(new java.awt.Color(204, 255, 255));
        pnlBoards.setEnabled(false);
        pnlBoards.setInheritsPopupMenu(true);
        pnlBoards.setOpaque(false);

        javax.swing.GroupLayout pnlBoardsLayout = new javax.swing.GroupLayout(pnlBoards);
        pnlBoards.setLayout(pnlBoardsLayout);
        pnlBoardsLayout.setHorizontalGroup(
            pnlBoardsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 508, Short.MAX_VALUE)
        );
        pnlBoardsLayout.setVerticalGroup(
            pnlBoardsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 593, Short.MAX_VALUE)
        );

        sclpnlBoards.setViewportView(pnlBoards);

        lblUsername.setEditable(false);
        lblUsername.setBackground(new java.awt.Color(204, 255, 255));
        lblUsername.setFont(new java.awt.Font("Tahoma", 2, 24)); // NOI18N
        lblUsername.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblUsername.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Username:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        lblTotalPlayingTime.setEditable(false);
        lblTotalPlayingTime.setBackground(new java.awt.Color(204, 255, 255));
        lblTotalPlayingTime.setFont(new java.awt.Font("Tahoma", 2, 24)); // NOI18N
        lblTotalPlayingTime.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblTotalPlayingTime.setText("---");
        lblTotalPlayingTime.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Total Playing Time:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        lblSolvedGames.setEditable(false);
        lblSolvedGames.setBackground(new java.awt.Color(204, 255, 255));
        lblSolvedGames.setFont(new java.awt.Font("Tahoma", 2, 24)); // NOI18N
        lblSolvedGames.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblSolvedGames.setText("---");
        lblSolvedGames.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Solved Games:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        lblCompletion.setEditable(false);
        lblCompletion.setBackground(new java.awt.Color(204, 255, 255));
        lblCompletion.setFont(new java.awt.Font("Tahoma", 2, 24)); // NOI18N
        lblCompletion.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblCompletion.setText("---");
        lblCompletion.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Completion:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        btnRandomGame.setBackground(new java.awt.Color(204, 204, 204));
        btnRandomGame.setFont(new java.awt.Font("Algerian", 0, 24)); // NOI18N
        btnRandomGame.setText("Random game");
        btnRandomGame.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnRandomGame.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRandomGame.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnRandomGameMouseClicked(evt);
            }
        });

        btnSolveMe.setBackground(new java.awt.Color(204, 204, 204));
        btnSolveMe.setFont(new java.awt.Font("Algerian", 0, 24)); // NOI18N
        btnSolveMe.setText("Solve me!");
        btnSolveMe.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnSolveMe.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSolveMe.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSolveMeMouseClicked(evt);
            }
        });

        chooseLevel.setFont(new java.awt.Font("Algerian", 0, 16)); // NOI18N
        chooseLevel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "easy", "medium", "hard" }));

        btnOrder.setBackground(new java.awt.Color(204, 204, 204));
        btnOrder.setFont(new java.awt.Font("Algerian", 0, 24)); // NOI18N
        btnOrder.setText("ORDER BY");
        btnOrder.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnOrder.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnOrder.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnOrderMouseClicked(evt);
            }
        });

        chooseOrder.setFont(new java.awt.Font("Algerian", 0, 16)); // NOI18N
        chooseOrder.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ALL BOARDS", "SAVED", "SOLVED", "UNSULVED" }));

        btnInstuctions.setFont(new java.awt.Font("Algerian", 3, 24)); // NOI18N
        btnInstuctions.setText("?");
        btnInstuctions.setToolTipText("<html>\n<p style=\"font-size:15px\">You can click on one of the boards\n<br>on the right to start a game.\n<br>on the right to start a game.\n<br>'SOLVE ME' - To type a board and the system will solve it.\n<br>\n<br>'RANDOM GAME' - You can choose a difficulty level\n<br>and the sysytem will create a new board according this rule \n<br>EASY: complete 20.\n<br>MEDIUM: complete 30.\n<br>HARD: comlete 40.\n<br>\n<br>'ORDER BY' - You can only see the boards by seletion (ALL BOARDS/ SOLVED/ UNSOLVED/ SAVED).</p>\n</html>\n\n"); // NOI18N
        btnInstuctions.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnInstuctions.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnInstuctionsMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblUsername)
                            .addComponent(lblTotalPlayingTime, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblSolvedGames, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblCompletion, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnOrder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(chooseOrder, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnRandomGame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnSolveMe, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(chooseLevel, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(sclpnlBoards, javax.swing.GroupLayout.PREFERRED_SIZE, 510, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(38, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(106, 106, 106)
                        .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnInstuctions)
                        .addGap(24, 24, 24))))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Background)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnInstuctions, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sclpnlBoards))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTotalPlayingTime, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSolvedGames, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblCompletion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34)
                        .addComponent(btnSolveMe)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRandomGame)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chooseLevel, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnOrder)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chooseOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(Background)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
        
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // new request loguot
        sc = new ServerConnection(Request.LOGOUT, player);
        sc.newServerRequest();
    }//GEN-LAST:event_formWindowClosing
    private void btnExitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnExitMouseClicked
        // new request loguot
        sc = new ServerConnection(Request.LOGOUT, player);
        sc.newServerRequest();
        
        // go to login screen
        LoginScreen ls = new LoginScreen();
        ls.setLocation(this.getLocation());
        ls.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnExitMouseClicked

    private void btnRandomGameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRandomGameMouseClicked
        Board newBoard = new Board();
        sc = new ServerConnection(Request.RANDOM_BOARD, newBoard, chooseLevel.getSelectedIndex());
        newBoard = (Board) sc.newServerRequest().getInfo();
        sc = new ServerConnection(Request.ADD_BOARD, newBoard);
        newBoard = (Board) sc.newServerRequest().getInfo();
        if (sc.getResult() == Result.SUCCESS) {
            GameScreen gs = new GameScreen(player, newBoard);
            gs.setLocation(this.getLocation());
            gs.setVisible(true);
            this.dispose();
        } else {
            System.out.println("ERROR");
        }
    }//GEN-LAST:event_btnRandomGameMouseClicked

    private void btnSolveMeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSolveMeMouseClicked
        GameScreen ss = new GameScreen(player);
        ss.setLocation(this.getLocation());
        ss.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnSolveMeMouseClicked

    private void btnOrderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnOrderMouseClicked
        lblTitle.setText((String)chooseOrder.getSelectedItem());
        orderBoards((String)chooseOrder.getSelectedItem());
    }//GEN-LAST:event_btnOrderMouseClicked

    private void btnInstuctionsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnInstuctionsMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnInstuctionsMouseClicked
  
    private void orderBoards(String status) {
        this.pnlBoards.removeAll();
        this.pnlBoards.revalidate();
        this.pnlBoards.repaint();

        for (Board board : boards) {
            if (status.equalsIgnoreCase("all boards") || board.getStatus().equalsIgnoreCase(status)) {
                this.pnlBoards.add(new pnlChooseBoard(this, board));
            }
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Background;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnInstuctions;
    private javax.swing.JButton btnOrder;
    private javax.swing.JButton btnRandomGame;
    private javax.swing.JButton btnSolveMe;
    private javax.swing.JComboBox<String> chooseLevel;
    private javax.swing.JComboBox<String> chooseOrder;
    private javax.swing.JTextField lblCompletion;
    private javax.swing.JTextField lblSolvedGames;
    private java.awt.Label lblTitle;
    private javax.swing.JTextField lblTotalPlayingTime;
    private javax.swing.JTextField lblUsername;
    private javax.swing.JPanel pnlBoards;
    private javax.swing.JScrollPane sclpnlBoards;
    // End of variables declaration//GEN-END:variables
}
