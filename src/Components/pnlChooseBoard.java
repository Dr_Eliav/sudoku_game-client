
package Components;

import ObjectsSerialization.Board;
import java.awt.Color;
import screens.GameScreen;
import screens.HomeScrenn;

/**
 *
 * @author EliavBuskila
 */
public class pnlChooseBoard extends javax.swing.JPanel {

    private Board board;
    private HomeScrenn hs;
    private Color lblBackground;
    
    public pnlChooseBoard() {
        initComponents();
    }
    public pnlChooseBoard(HomeScrenn hs, Board board) {
        initComponents();
        this.hs = hs;
        this.lblBackground = new Color(204, 255, 255);
        this.board = new Board(board);
        lblBoardID.setText(board.getBoardID()+"");
        lblNumberOfSolvers.setText(board.getNumberOfSolvers()+"");
        lblBestTime.setText(board.getBestTime());
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblNumberOfSolvers = new javax.swing.JTextField();
        lblBestTime = new javax.swing.JTextField();
        lblBoardID = new javax.swing.JTextField();
        btnBoard = new javax.swing.JButton();

        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMaximumSize(new java.awt.Dimension(500, 200));
        setOpaque(false);

        lblNumberOfSolvers.setEditable(false);
        lblNumberOfSolvers.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNumberOfSolvers.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblNumberOfSolvers.setText("0");
        lblNumberOfSolvers.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Number of solvers:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        lblBestTime.setEditable(false);
        lblBestTime.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblBestTime.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblBestTime.setText("---");
        lblBestTime.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Best time:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        lblBoardID.setEditable(false);
        lblBoardID.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblBoardID.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblBoardID.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Board ID:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        btnBoard.setIcon(new javax.swing.ImageIcon("C:\\Users\\EliavBuskila\\Desktop\\Study\\Sudoku_Game\\JAVA\\image\\choose baord.png")); // NOI18N
        btnBoard.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBoard.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBoardMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnBoardMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnBoardMouseExited(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBoard, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblBoardID, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNumberOfSolvers, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblBestTime, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 15, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnBoard, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblBoardID, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblNumberOfSolvers, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblBestTime, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 13, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBoardMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBoardMouseClicked
        GameScreen gs = new GameScreen(hs.getPlayer(), this.board);
        gs.setLocation(hs.getLocation());
        gs.setVisible(true);
        hs.dispose();
    }//GEN-LAST:event_btnBoardMouseClicked

    private void btnBoardMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBoardMouseEntered
        lblBoardID.setBackground(lblBackground);
        lblNumberOfSolvers.setBackground(lblBackground);
        lblBestTime.setBackground(lblBackground);
    }//GEN-LAST:event_btnBoardMouseEntered

    private void btnBoardMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBoardMouseExited
        lblBoardID.setBackground(Color.WHITE);
        lblNumberOfSolvers.setBackground(Color.WHITE);
        lblBestTime.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnBoardMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBoard;
    private javax.swing.JTextField lblBestTime;
    private javax.swing.JTextField lblBoardID;
    private javax.swing.JTextField lblNumberOfSolvers;
    // End of variables declaration//GEN-END:variables
}
